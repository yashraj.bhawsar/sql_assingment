
           /*

                      Exercise.4. Create a function that takes as inputs a SalesOrderID, a Currency Code,

                                              and a date, and returns a table of all the SalesOrderDetail rows for

                                              that Sales Order including Quantity, ProductID, UnitPrice, and the unit

                                              price converted to the target currency based on the end of day rate for

                                              the date provided. Exchange rates can be found in the Sales.CurrencyRate

                                              table. ( Use AdventureWorks)

           */

          

 

           Go

           Create function New2Function(@Sales_OrderId int, @Currency_Code nvarchar(3), @Date datetime)

           Returns table

           As

           Return

                      (Select sd.ProductID ,

                                    sd.OrderQty ,

                                    sd.UnitPrice ,

                                    sd.UnitPrice*sr.EndOfDayRate AS 'Target Price'

                      From Sales.SalesOrderDetail As sd,

                                 Sales.CurrencyRate As sr

                      Where sr.ToCurrencyCode = @Currency_Code AND

                                   sr.ModifiedDate = @Date AND

                                   sd.SalesOrderID = @Sales_OrderID)

           Go

 

           Select * from New2Function(43665,'ARS','2005-07-01');

          

           drop Function New2Function

          

 

          

 

                                                                                                                       

          

