  /*

                      Exercise.5. Write a Procedure supplying name information from the Person.

                                              Person table and accepting a filter for the first name.

                                              Alter the above Store Procedure to supply Default Values

                                              if user does not enter any value.( Use AdventureWorks).

           */

          

 

          

           GO

                      Create procedure SupplyingNameInfFromPerson

 

                      @FirstName nvarchar(20) = 'Sam'

           As

           Begin

                      Select BusinessEntityID ,

                                   CONCAT(FirstName,LastName) As 'NAME',

                                    PersonType

                      From Person.Person

                      Where FirstName = @FirstName

                      End

           GO

 

           Execute SupplyingNameInfFromPerson

 

           Execute SupplyingNameInfFromPerson @FirstName = 'Zoe'

 

           Drop Procedure SupplyingNameInfFromPerson

 

 

          

          

 

          

 

                                                                                                                       

          