/*                  Exercise.2. Write separate queries to list all AdventureWorks customers who

                      have not placed an order.

           */

          

 

           -- 2.1:: By Using JOIN Statement

          

 

           SELECT Pers.FirstName + Pers.LastName AS Customer_Name

           FROM Person.Person Pers INNER JOIN

                      Sales.Customer SC ON

                      Pers.BusinessEntityID = SC.CustomerID LEFT JOIN

                      Sales.SalesOrderHeader Sh ON

                      SC.CustomerID = Sh.CustomerID

           WHERE Sh.SalesOrderID IS NULL;

          

 

           -- 2.2:: By Using SubQuery

          

 

           SELECT FirstName + LastName AS Customer_Name

           FROM Person.Person

           Where BusinessEntityID IN (SELECT CustomerID

                                                                                          FROM Sales.Customer

                                                                                          WHERE CustomerID NOT IN  (SELECT CustomerID

                                                                                                                                                                       FROM Sales.SalesOrderHeader));

          

 

          
